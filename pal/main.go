package pal

import (
	"bufio"
	"io"
	"math/rand"
	"os"
)

func ReadLine(file string, lineNum int) (line string, err error) {
	f, err := os.Open(file)
	if err != nil && err != io.EOF {
		return "", err
	}
	r := bufio.NewReader(f)
	return ReadLineFromReader(r, lineNum)
}

func ReadLineFromReader(r io.Reader, lineNum int) (line string, err error) {
	sc := bufio.NewScanner(r)
	i := 0
	for sc.Scan() {
		i++
		if i == lineNum {
			return sc.Text(), sc.Err()
		}
	}
	return "", io.EOF
}

func LineCount(file string) (lineCount int, err error) {
	f, err := os.Open(file)
	if err != nil {
		return -1, err
	}
	r := bufio.NewReader(f)
	lineCount = LineCountFromReader(r)
	return lineCount, nil
}

func LineCountFromReader(r io.Reader) int {
	sc := bufio.NewScanner(r)
	i := 0
	for sc.Scan() {
		i++
	}
	return i
}

func RandomLine(file string) (string, error){
	lineCount, err := LineCount(file)
	if err != nil {
		return "", err
	}
	line := rand.Intn(lineCount + 1)
	s, err := ReadLine(file, line)
	if err != nil {
		return "", err
	}
	return s, nil
}
