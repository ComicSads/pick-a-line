// +build mage

package main

import (
	"fmt"
	"os"
	"os/exec"
	//"github.com/magefile/mage/mg" // mg contains helpful utility functions, like Deps
)

// Builds executable named "build"
func Build() error {
	cmd := exec.Command("go", "build", "-o", "build", ".")
	return cmd.Run()
}

// Removes build exec and installs exec
func Install() error {
	err := os.RemoveAll("build")
	if err != nil {
		return err
	}
	cmd := exec.Command("go", "install")
	return cmd.Run()
}
