package main

import (
	"bufio"
	"fmt"
	"gitlab.com/ComicSads/pick-a-line/pal"
	"os"
	"strconv"
)

func main() {
	switch len(os.Args) {
	case 1:
		fmt.Fprintln(os.Stderr, "Please provide a file and line number")
		os.Exit(1)
	case 2:
		r := bufio.NewReader(os.Stdin)
		line, err := strconv.Atoi(os.Args[1])
		if err != nil {
			fmt.Fprintln(os.Stderr, "Line number '"+os.Args[1]+"' does not appear to be a valid integer")
			os.Exit(1)
		}
		s, err := pal.ReadLineFromReader(r, line)
		if err != nil {
			os.Exit(1)
		}
		fmt.Println(s)
	case 3:
		line, err := strconv.Atoi(os.Args[2])
		if err != nil {
			fmt.Fprintln(os.Stderr, "Line number does not appear to be a valid integer")
			os.Exit(1)
		}
		s, err := pal.ReadLine(os.Args[1], line)
		if err != nil {
			os.Exit(1)
		}
		fmt.Println(s)
	}
}
